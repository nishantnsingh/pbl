<%@page language="java"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>

<jsp:include page="tDetails.html" />

<%!
  Connection con;
  PreparedStatement ps;
  PreparedStatement ps1;
  ResultSet rs;
%>  

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Automated Checker</title>
  
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>

	<div class="group_grid">
	 <form  method="post" action = "AutomatedCheckerConf.jsp" class="form group">

      <header class="group__header">
        <h3 class="group__title">Group Chat</h3>
      </header>

      <div class="group__body">
        <div class="form__field">
<%
	String answer; //to get the answer from database
	String group_name = request.getParameter("group_name");
	Integer totalCount = 0; //to get the total count of all the words that are present in the answer
	Integer wordCount = 0; //to get the word count
	try
	{
	  Class.forName("com.mysql.jdbc.Driver");
      con=DriverManager.getConnection("jdbc:mysql:///db_pbls","root","root1234");
      ps=con.prepareStatement("select solution from studentSol where group_name = ?");
      ps.setString(1, group_name);
      rs = ps.executeQuery();
      if(rs.next())
      {
      	%>
      	  <input type="text" readonly required name="group_name" value=<%=request.getParameter("group_name")%>>

      	<textarea class = "textarea" rows="15" cols="143" readonly>
      		<%=rs.getString("solution")%>
      	</textarea>

        <%

      		answer = rs.getString("solution");
      		Scanner text = new Scanner(answer);
      		List<String> words = new ArrayList<String>();
      			words.add("algorithms");words.add("a");words.add("an");words.add("the");words.add("computer");words.add("is");words.add("are");words.add("software");words.add("engineering");words.add("of");words.add("softwares");words.add("android");words.add("database");words.add("mysql");words.add("jpa");words.add("entity");words.add("modelling");words.add("unit testing");words.add("integration testing");words.add("programming");words.add("junit testing");words.add("junit");words.add("android");words.add("its");words.add("get");words.add("all");words.add("html");words.add("css");words.add("java");words.add("jdk");words.add("jsp");words.add("servlet");words.add("connectivity");words.add("dependency");words.add("phone");words.add("bluetooth");
      			words.add("record");words.add("table");words.add("column");words.add("query");words.add("compiler");words.add("interpreter");words.add("programmer");words.add("how");words.add("why");words.add("what");words.add("where");words.add("their");words.add("there");words.add("who");
      			words.add("daily");words.add("scrum");words.add("sprint");words.add("xp");words.add("agile");
      			words.add("waterfall");words.add("defect analysis");words.add("cost analysis");words.add("risk analysis");words.add("user interface");words.add("modelling");words.add("class");words.add("object");words.add("unified");words.add("compare");words.add("string");words.add("integer");words.add("text");words.add("word");words.add("for");words.add("loop");words.add("while");words.add("do while");words.add("switch");words.add("coding");words.add("testing");words.add("c++");words.add("c");words.add("c#");words.add("python");words.add("has");words.add("have");words.add("had");words.add("requirements");words.add("requirement");words.add("documentation");words.add("srs");words.add("person");words.add("place");words.add("thing");words.add("get");words.add("just");words.add("water");words.add("wall");words.add(",");words.add(".");words.add("?");words.add("*");words.add("&");words.add("knowledge");words.add("persons");words.add("words");words.add("plathora");words.add("spring");words.add("web");words.add("mvc");words.add("data");words.add("centre");words.add("cloud computing");words.add("jquery");words.add("json");words.add("postman");words.add("rest");words.add("service");words.add("controller");words.add("repositories");words.add("repository");words.add("mapper");words.add("domain");words.add("dto");words.add("testing");words.add("tester");words.add("black box");words.add("white box");
      			words.add("sellinium");words.add("automated");words.add("user");

      			while(text.hasNext())
      			{
      				totalCount++;
      				String nextToken = text.next();
      				for(String k: words)
      				{
      					if(nextToken.equalsIgnoreCase(k))
      					{
      						wordCount++;
      					}
      				}
      			}
      			Long marks = Math.round((((double)wordCount/totalCount)*100) + 20);
      	%>
      			 
      	<input type="text" readonly required name="marks" value="<%out.println(Math.round((((double)wordCount/totalCount)*100) + 20) + "%");
	%>">
      	
      	  <input type="text" placeholder="Enter name to confirm...." required name="teacher_name">

      	
    
</div>
</div>
  
           <footer class="group__footer">
        <input type="submit" value="Click to generate marks">
        </footer>


<%
      		/*out.println("Word Count: " + wordCount);
      		out.println("Total Count: " + totalCount);*/
      		
      }
	}
	catch(Exception e)
	{
		out.print(e);
	} 
%>
</form>
</div>
</body>
</html>