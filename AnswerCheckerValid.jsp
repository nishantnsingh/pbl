
<jsp:include page="tDetails.html" />


<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>
  
  
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
  <div class="group_grid">

    <form  method="post" action="AutomatedChecker.jsp" class="form group">

      <header class="group__header">
        <h3 class="group__title">View Group Chat</h3>
      </header>

      <div class="group__body">
        <div class="form__field">
          <input type="text" placeholder="Enter your group name" required name="group_name">
        </div>
      </div>
        <footer class="group__footer">
        <input type="submit" value="Click to go to Group Chat">
        </footer>
    </form>
  </div>
 </body>
</html>
