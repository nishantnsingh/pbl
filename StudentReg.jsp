<%@page language="java"%>
<%@page import="java.sql.*"%>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Student Registeration</title>
  
  
  
      <link rel="stylesheet" href="css/style.css">

  <script type="text/javascript">
    function jsFunction(){
   // set text box value here
   var txt =  document.getElementById('txtBox');
   txt.value = "assign_here";
}
  </script>
</head>

<body>
  <body class="align">

  <div class="grid1">

    <form action="sRegValidation.jsp" method="post" class="form login">

      <header class="login__header">
        <h3 class="login__title">Student Registration</h3>
      </header>

      <div class="login__body">

        <div class="form__field">
          <input type="email" placeholder="Email" required name="email">
        </div>

        <div class="form__field">
          <input type="password" placeholder="Password" required name="password">
        </div>

        <div class="form__field">
          <input type="text" placeholder="Registration Number" required name="reg_no">
        </div>

        <div class="form__field">
          <input type="text" placeholder="Name" required name="name">
        </div>

        <div class="form__field">
          <input type="text" placeholder="Department" required name="department">
        </div>           
        <%
Connection con = null;
PreparedStatement ps = null;
try
{
Class.forName("com.mysql.jdbc.Driver");
con = DriverManager.getConnection("jdbc:mysql:///db_pbls","root","root1234");
String sql = "SELECT tname FROM teacher";
ps = con.prepareStatement(sql);
ResultSet rs = ps.executeQuery(); 
%>
<div class="form__field">
 <select class="select" title="select" name="teacher" required>
  <option value="" selected disabled hidden class="select_name" >Select Teacher</option></div>
<%
while(rs.next())
{
String name = rs.getString("tname"); 
%>
<option value="<%=name %>" placeholder="Select Teacher"><%=name %></option>
<%
}
%>
</select>
</div>
<%
}
catch(SQLException sqe)
{ 
out.println(sqe);
}
%>
     <br><br><br><br> <footer class="login__footer">

        <input type="reset" value="RESET">
         <p><span class="icon icon--info"><</span><a href="StudentLogin.html">Back to Login Page</a></p>
        <input type="submit" value="SUBMIT">
      </footer>

    </form>

  </div>

</body>
  
</body>

</html>
