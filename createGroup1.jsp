
<%@page language="java"%>
<%@page import="java.sql.*"%>

<jsp:include page="tDetails.html" />

<%!
  Connection con;
  PreparedStatement ps;
  ResultSet rs;
%>  

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>
   
  
  
      <link rel="stylesheet" href="css/style.css">
</head>

<body>

  <div class="group_grid">
    <form action = "groupCreator.jsp" method="post" class="form login">
      <header class="group__header">
        <h3 class="group__title">Create group</h3>
      </header>
      <div class="group__body">
        <input type="text" placeholder="Enter group name" name="group_name" required>
        <input type="text" name="team_owner" placeholder="Enter your name" required>
      </div>
      
      <div class="group__body">

    <select multiple name="group_members">
<%  
    
    String id=request.getParameter("teacher_id");
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
      con=DriverManager.getConnection("jdbc:mysql:///db_pbls","root","root1234");
      ps=con.prepareStatement("select name from student JOIN teacher ON student.teacher = teacher.tname and teacher.teacher_id=?");
      ps.setString(1, id);
      rs=ps.executeQuery();
      while(rs.next())
      { 
        String n = rs.getString("name");
%>
          <option value="<%=n%>" ><%=n%></option>

<%    }
%>
</select>   
      </div>
      <%
    }
  catch(Exception e)
  {
      out.print(e);
  }
%> 
      <footer class="group__footer">
        <input type="submit" value="Create group">
      </footer>

</form>
  </div>
 </body>
</html>
