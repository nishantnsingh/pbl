
<jsp:include page="sDetail.html" />


<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>
  
  
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
  <div class="group_grid">

    <form  method="post" action = "submitSolValid.jsp" class="form group">

      <header class="group__header">
        <h3 class="group__title">Submit Solution</h3>
      </header>

      <div class="group__body">
         <input type="text" placeholder="Enter group name" required name="group_name"><br>
         <br><select name="plugin_used" class="select">
  <option value="yes">YES</option>
  <option value="no">NO</option>
</select>
<br>
         
        <br> <textarea class="textarea" rows="10"c cols="140" name="solution" required placeholder="Enter solution here"></textarea>
        </div>
        <footer class="group__footer">
        <input type="submit" value="Click to submit solution">
        </footer>
    </form>
  </div>
 </body>
</html>
