
<%@page language="java"%>
<%@page import="java.sql.*"%>

<jsp:include page="sDetail.html" />

<%!
  Connection con;
  PreparedStatement ps;
  PreparedStatement ps1;
  PreparedStatement ps2;
  ResultSet rs;
  ResultSet rs1;
  ResultSet rs2;
%>  

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>
   
  
  
      <link rel="stylesheet" href="css/style.css">
</head>

<body>
   <div class="group_grid">
    <form method="post" class="form login" action="sDetail.html">
      <header class="group__header">
        <h3 class="group__title">Detailed result</h3> <div class="group_grid">
      </header>
      <div class="group__body">
        

<%
    try
    {
      String group_name = request.getParameter("group_name");
      Class.forName("com.mysql.jdbc.Driver");
      con=DriverManager.getConnection("jdbc:mysql:///db_pbls","root","root1234");
      ps=con.prepareStatement("select * from message where group_name = ?");
      ps.setString(1, group_name);
      rs = ps.executeQuery();
      ps1 = con.prepareStatement("select * from groups where group_name = ?");
      ps1.setString(1, group_name);
      rs1 = ps1.executeQuery();
      ps2 = con.prepareStatement("select question from assignQuestion where group_name = ?");
      ps2.setString(1, group_name);
      rs2 = ps2.executeQuery();
      if(rs.next() && rs1.next() && rs2.next())
      {
%>
           <div class="form__field">
         <h3><strong>Group Name: </strong></h3><input type="text" readonly required name="group_name" value="<%=rs.getString("group_name")%>">

         <h3><strong>Group Members: </strong></h3><input type="text" readonly required name="group_members" value="<%=rs1.getString("group_members")%>" >

        <h3><strong>Question: </strong></h3><input type="text" readonly required name="group_members" value="<%=rs2.getString("question")%>" > 

         <h3><strong>Marks: </strong></h3><input type="text" readonly required name="marks" value="<%=rs.getString("marks")%>">

         <h3><strong>Teacher Name: </strong></h3><input type="text" readonly required name="teacher_name" value="<%=rs.getString("teacher_name")%>">
        </div>      
<%
      }
      
    }
    catch(Exception e){
    out.println(e);
  }
%>
      </div>

    <footer class="group__footer">
        <input type="submit" value="Click to go back">
        </footer>
    </form>
  </div>
 </body>
</html>
